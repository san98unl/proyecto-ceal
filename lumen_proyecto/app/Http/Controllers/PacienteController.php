<?php

namespace App\Http\Controllers;

use App\Paciente;

use App\Doctor;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\BcryptHasher;


class PacienteController extends BaseController
{
	
	public function all(Request $request){
    	$paciente = Paciente::all();
    	return response()->json($paciente,200);
    }
		}