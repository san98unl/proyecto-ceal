<?php

namespace App\Http\Controllers;


use App\Doctor;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\BcryptHasher;


class DoctorController extends BaseController
{
	
	public function all(Request $request){
    	$doctor = Doctor::all();
    	return response()->json($doctor,200);
    }
		}