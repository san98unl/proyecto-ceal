<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Paciente extends Model {
   protected $table = 'modelo_paciente';
   protected $primaryKey = 
         'paciente_id'
    ;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'cedula', 'nombres', 'apellidos', 'genero', 'doctor_id'
    ];


    public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   /*
    protected $hidden = [
        'password',
    ];

*/
}