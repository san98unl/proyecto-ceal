<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Doctor extends Model {
   protected $table = 'modelo_doctor';
 protected $primaryKey = 
         'doctor_id'
    ;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    

    protected $fillable = [
        'cedula', 'nombres', 'apellidos', 'genero', 'fechaNacimiento', 'correo',
        'telefono', 'celular', 'direccion'
    ];


    public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   /*
    protected $hidden = [
        'password',
    ];

*/
}