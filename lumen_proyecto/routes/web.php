<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->group(['prefix'=>'doctor'],function($router){
	$router->get('/all','DoctorController@all');	

});

$router->group(['prefix'=>'paciente'],function($router){
	$router->get('/all','PacienteController@all');	

});