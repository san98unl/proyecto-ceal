from django.apps import AppConfig


class PictogramaConfig(AppConfig):
    name = 'apps.pictograma'
