from django.shortcuts import render, redirect
from apps.modelo.models import Persona,Rol
from .forms import FormularioRol
#Para Buscar
from django.db.models import Q
#from django.shortcuts import render_to_response
#Para login
from django.contrib.auth.decorators import login_required

# Create your views here.

def principal(request):
	

	return render (request,'principal.html')#reder redireccion, rempitanta un docuemto html en forma grafica,,,escribimos el nombre de nuestra url

def asignar_rol(request):
	dni = request.GET['cedula']
	persona = Persona.objects.get(cedula = dni)
	formulario = FormularioRol(request.POST)	
	if request.method == 'POST':
		if formulario.is_valid():
			if not Rol.objects.get(Persona_id =persona.persona_id):
				datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
				rol=Rol() #asi se crear los objetos en python
				rol.nombre=datos.get('nombre')
				rol.persona_id=persona.persona_id
				#mensaje="ya existe un rol para esa persona"
				rol.save()
			#else:
				#mensaje="ya existe un rol para esa persona"

				

	return redirect(principal)
	#else:
			#return render (request,'cliente/acceso_prohibido.html')	
	context={
			'f':formulario,
			'mensaje':mensaje,
			

	}
	return render (request,'cuenta/asignar_rol.html', context)

def busqueda(request):
    query = request.GET.get('q', '')

    print(query)
    if query:
        qset = (
            Q(cedula=query) 
        )

        results = Persona.objects.filter(qset).distinct()
    else:
        results = []
    return render("cuenta/buscar.html", {
        "results": results,
        "query": query
    })