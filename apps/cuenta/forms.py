from django import forms
from django.forms import  ClearableFileInput
from apps.modelo.models import Rol


from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField

class FormularioRol(forms.ModelForm):
	class Meta:
		model = Rol
		fields=["nombre"]#lo que voy a representar en la parte grafica
		widgets = {
		
		'nombre' : forms.Select(attrs={'class':'form-control'}),
		
		}