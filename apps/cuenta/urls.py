from django.urls import path
from .import views


urlpatterns = [
	path('',views.principal, name="doctor"),
	path('asignar_rol/',views.asignar_rol),
	path('buscar/',views.busqueda),

   
    #path('crear_medico',views.crear),
    
]