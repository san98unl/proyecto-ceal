from django.shortcuts import render, redirect
from apps.modelo.models import Doctor,Paciente, Pictograma,Imagen,Audio
from .forms import FormularioMedico, FormularioModificarMedico, FormularioAudio, FormularioImagen
#Para login
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def principal(request):
	

	return render (request,'principal.html')#reder redireccion, rempitanta un docuemto html en forma grafica,,,escribimos el nombre de nuestra url
@login_required
def imagenes(request):
	lista=Imagen.objects.all()

	return render (request,'doctor/ver_media.html',{'lista_imagenes': lista})#reder redireccion, rempitanta un docuemto html en forma grafica,,,escribimos el nombre de nuestra url
@login_required
def audios(request):
	lista=Audio.objects.all()

	return render (request,'doctor/ver_audios.html',{'lista_audios': lista})#reder redireccion, rempitanta un docuemto html en forma grafica,,,escribimos el nombre de nuestra url


def crear_doctor(request):
	formulario = FormularioMedico(request.POST)
		
	 
		
	if request.method == 'POST':
		if formulario.is_valid():
				datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
				doctor=Doctor() #asi se crear los objetos en python
				doctor.cedula=datos.get('cedula')
				doctor.nombres=datos.get('nombres')
				doctor.apellidos=datos.get('apellidos')
				doctor.genero=datos.get('genero')
				
				doctor.fechaNacimiento=datos.get('fechaNacimiento')
				doctor.correo=datos.get('correo')
				doctor.telefono=datos.get('telefono')
				doctor.celular=datos.get('celular')
				doctor.direccion=datos.get('direccion')
				doctor.save()
				

		return redirect(principal)
	#else:
			#return render (request,'cliente/acceso_prohibido.html')	
	context={
			'f':formulario,
			

	}
	return render (request,'doctor/crear_doctor.html', context)
@login_required
def listar_doctores(request):
	lista=Doctor.objects.all()


	return render(request,'doctor/listar_doctores.html',{'lista_doctores': lista})
#
@login_required
def eliminar_doctor(request):
	dni = request.GET['doctor_id']
	doctor = Doctor.objects.get(doctor_id = dni)
	doctor.delete()
	return redirect(listar_doctores)
@login_required
def modificar_doctor(request):
	dni = request.GET['cedula']
	doctores = Doctor.objects.get(cedula = dni)
	if request.method == 'POST': 
		formulario = FormularioModificarMedico(request.POST)
		if formulario.is_valid():
			datos=form-ulario.cleaned_data #obteniendo todos los datos del formulario cliente
			doctores.nombres=datos.get('nombres')
			doctores.apellidos=datos.get('apellidos')
			doctores.genero=datos.get('genero')
			doctores.fechaNacimiento=datos.get('fechaNacimiento')
			doctores.correo=datos.get('correo')
			doctores.telefono=datos.get('telefono')
			doctores.celular=datos.get('celular')
			doctores.direccion=datos.get('direccion')
			doctores.save()
			return redirect(principal)

		#se modifica
		
	else:
		formulario = FormularioMedico(instance = doctores)
	context = {
		'dni': dni,
		'doctor' : doctores,
		'formulario': formulario
	}
	return render (request,'doctor/modificar_doctor.html', context)
@login_required
def guardar_audio(request):
	formulario = FormularioAudio(request.POST, request.FILES)
	if request.method == 'POST':
		if formulario.is_valid():
				audio = formulario.save(commit=False)

				audio.doctor_id = 1
				

				audio.save()
				return redirect(audios)


	#else:
			#return render (request,'cliente/acceso_prohibido.html')	
	context = {
			'f':formulario,
			

	}

	return render (request,'doctor/guardar_audio.html', context)
	#return render_to_response('pictograma/upload.html', locals(), context_instance=RequestContext(request))

@login_required
def guardar_imagen(request):
	formulario = FormularioImagen(request.POST, request.FILES)
	if request.method == 'POST':
		if formulario.is_valid():
				imagen = formulario.save(commit=False)

				imagen.doctor_id = 1
				

				imagen.save()
				return redirect(imagenes)


	#else:
			#return render (request,'cliente/acceso_prohibido.html')	
	context = {
			'f':formulario,
			

	}

	return render (request,'doctor/guardar_imagen.html', context)
	#return render_to_response('pictograma/upload.html', locals(), context_instance=RequestContext(request))



