from django.urls import path
from .import views


urlpatterns = [
	path('',views.principal, name="doctor"),
    path('crear_doctor',views.crear_doctor),
    path('listar_doctor',views.listar_doctores),
    path('imagenes',views.imagenes),
    path('audios',views.audios),
    path('modificar_doctor/',views.modificar_doctor),
    path('eliminar_doctor/',views.eliminar_doctor),

     path('guardar_imagen/',views.guardar_imagen),
     path('guardar_audio/',views.guardar_audio),
    
    
    #path('crear_medico',views.crear),
    
]
