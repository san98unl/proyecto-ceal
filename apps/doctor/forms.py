from django import forms
from django.forms import  ClearableFileInput
from apps.modelo.models import Doctor
from apps.modelo.models import Paciente
from apps.modelo.models import Imagen,Audio

from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField


#por cada metodo se coloca un url..
class FormularioMedico(forms.ModelForm):
	class Meta:
		model = Doctor
		fields=["cedula","nombres","apellidos","fechaNacimiento",
		"genero","correo","telefono","celular",
		"direccion"]#lo que voy a representar en la parte grafica
		widgets = {
		'cedula' : forms.TextInput(attrs={'class':'form-control'}),
		'nombres' : forms.TextInput(attrs={'class':'form-control'}),
		'apellidos' : forms.TextInput(attrs={'class':'form-control'}),
		'fechaNacimiento' :forms.TextInput(attrs={'class':'form-control'}),
		'genero' : forms.Select(attrs={'class':'form-control'}),
		'correo' : forms.TextInput(attrs={'class':'form-control'}),
		'telefono' : forms.TextInput(attrs={'class':'form-control'}),
		'celular' : forms.TextInput(attrs={'class':'form-control'}),
		'direccion' : forms.TextInput(attrs={'class':'form-control'}),
		}
class FormularioModificarMedico(forms.ModelForm):
	class Meta:
		model = Doctor
		fields=["nombres","apellidos","fechaNacimiento",
		"genero","correo","telefono","celular",
		"direccion"]#lo que voy a representar en la parte grafica

class FormularioImagen(forms.ModelForm):
    class Meta:
        model = Imagen
        fields = ('titulo', 'descripcion', 'foto')
        #widgets = {
        #    'archivo': CustomClearableFileInput
        #}	

class FormularioAudio(forms.ModelForm):
    class Meta:
        model = Audio
        fields = ('titulo', 'descripcion', 'audio')	
        
