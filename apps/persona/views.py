from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from apps.modelo.models import Persona,Rol,Imagen,Audio
from django.urls import reverse
from .forms import FormularioPersona,FormularioRol,FormularioAudio,FormularioImagen
from .forms import FormularioModificarPersona

from django.contrib.auth.decorators import login_required

#Muestra la venta principal
def principal(request):
	return render(request,'principal.html')
#Metodo que crea una persona x defecto tiene rol paciente
@login_required
def crear_persona(request):
	formulario = FormularioPersona(request.POST)
		
	 
		
	if request.method == 'POST':
		if formulario.is_valid():
				datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
				persona=Persona() #asi se crear los objetos en python
				persona.cedula=datos.get('cedula')
				persona.nombres=datos.get('nombres')
				persona.apellidos=datos.get('apellidos')
				persona.genero=datos.get('genero')
				
				persona.fechaNacimiento=datos.get('fechaNacimiento')
				persona.correo=datos.get('correo')
				persona.telefono=datos.get('telefono')
				persona.celular=datos.get('celular')
				persona.direccion=datos.get('direccion')
				persona.save()
				

		return redirect(principal)
	#else:
			#return render (request,'cliente/acceso_prohibido.html')	
	context={
			'f':formulario,
			

	}
	return render (request,'persona/crear_persona.html', context)
##Muestra la venta principal
@login_required
def listar_personas(request):
	lista=Persona.objects.all()
	return render(request,'persona/listar.html',{'lista_personas': lista})

def imagenes(request):
	lista=Imagen.objects.all()

	return render (request,'doctor/ver_media.html',{'lista_imagenes': lista})#reder redireccion, rempitanta un docuemto html en forma grafica,,,escribimos el nombre de nuestra url

def audios(request):
	lista=Audio.objects.all()

	return render (request,'doctor/ver_audios.html',{'lista_audios': lista})#reder redireccion, rempitanta un docuemto html en forma grafica,,,escribimos el nombre de nuestra url
@login_required
def asignar_rol(request):
	dni = request.GET['cedula']
	persona = Persona.objects.get(cedula = dni)
	formulario = FormularioRol(request.POST)	
	if request.method == 'POST':
		if formulario.is_valid():
			
				datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
				
				persona.rol_id=datos.get('nombre')
				
				#mensaje="ya existe un rol para esa persona"
				persona.save()
			#else:
				#mensaje="ya existe un rol para esa persona"

				

		return redirect(principal)
	#else:
			#return render (request,'cliente/acceso_prohibido.html')	
	context={
			'f':formulario,
			
			

	}
	return render (request,'cuenta/asignar_rol.html', context)
@login_required
def busqueda(request):
    query = request.GET.get('q', '')

    print(query)
    if query:
        qset = (
            Q(cedula=query) 
        )

        results = Persona.objects.filter(qset).distinct()
    else:
        results = []
    return render("cuenta/buscar.html", {
        "results": results,
        "query": query
    })
@login_required
def guardar_audio(request):
	formulario = FormularioAudio(request.POST, request.FILES)
	if request.method == 'POST':
		if formulario.is_valid():
				audio = formulario.save(commit=False)

				audio.persona_id = 2
				

				audio.save()
				return redirect(audios)


	#else:
			#return render (request,'cliente/acceso_prohibido.html')	
	context = {
			'f':formulario,
			

	}

	return render (request,'doctor/guardar_audio.html', context)
	#return render_to_response('pictograma/upload.html', locals(), context_instance=RequestContext(request))


@login_required
def guardar_imagen(request):
	formulario = FormularioImagen(request.POST, request.FILES)
	if request.method == 'POST':
		if formulario.is_valid():
				imagen = formulario.save(commit=False)

				imagen.persona_id = 2
				

				imagen.save()
				return redirect(imagenes)


	#else:
			#return render (request,'cliente/acceso_prohibido.html')	
	context = {
			'f':formulario,
			

	}

	return render (request,'doctor/guardar_imagen.html', context)
	#return render_to_response('pictograma/upload.html', locals(), context_instance=RequestContext(request))
def eliminar_persona(request):
	dni = request.GET['persona_id']
	persona = Persona.objects.get(persona_id = dni)
	persona.delete()
	return redirect(listar_personas)
@login_required
def modificar_persona(request):
	dni = request.GET['cedula']
	persona = Persona.objects.get(cedula = dni)
	if request.method == 'POST': 
		formulario = FormularioModificarPersona(request.POST)
		if formulario.is_valid():
			datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
			persona.nombres=datos.get('nombres')
			persona.apellidos=datos.get('apellidos')
			persona.genero=datos.get('genero')
			persona.fechaNacimiento=datos.get('fechaNacimiento')
			persona.correo=datos.get('correo')
			persona.telefono=datos.get('telefono')
			persona.celular=datos.get('celular')
			persona.direccion=datos.get('direccion')
			persona.save()
			return redirect(principal)

		#se modifica
		
	else:
		formulario = FormularioPersona(instance = persona)
	context = {
		'dni': dni,
		'persona' : persona,
		'formulario': formulario
	}
	return render (request,'persona/modificar_persona.html', context)
