from django import forms
#####
from django.forms import  ClearableFileInput
from apps.modelo.models import Persona,Rol,Imagen,Audio


from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField

class FormularioPersona(forms.ModelForm):
	class Meta:
		model = Persona
		fields=["cedula","nombres","apellidos","fechaNacimiento",
		"genero","correo","telefono","celular",
		"direccion"]#lo que voy a representar en la parte grafica
		widgets = {
		'cedula' : forms.TextInput(attrs={'class':'form-control'}),
		'nombres' : forms.TextInput(attrs={'class':'form-control'}),
		'apellidos' : forms.TextInput(attrs={'class':'form-control'}),
		'fechaNacimiento' :forms.TextInput(attrs={'class':'form-control'}),
		'genero' : forms.Select(attrs={'class':'form-control'}),
		'correo' : forms.TextInput(attrs={'class':'form-control'}),
		'telefono' : forms.TextInput(attrs={'class':'form-control'}),
		'celular' : forms.TextInput(attrs={'class':'form-control'}),
		'direccion' : forms.TextInput(attrs={'class':'form-control'}),
		}
class FormularioModificarPersona(forms.ModelForm):
	class Meta:
		model = Persona
		fields=["nombres","apellidos","fechaNacimiento",
		"genero","correo","telefono","celular",
		"direccion"]#lo que voy a representar en la parte grafica
		widgets = {
		
		'nombres' : forms.TextInput(attrs={'class':'form-control'}),
		'apellidos' : forms.TextInput(attrs={'class':'form-control'}),
		'fechaNacimiento' :forms.TextInput(attrs={'class':'form-control'}),
		'genero' : forms.Select(attrs={'class':'form-control'}),
		'correo' : forms.TextInput(attrs={'class':'form-control'}),
		'telefono' : forms.TextInput(attrs={'class':'form-control'}),
		'celular' : forms.TextInput(attrs={'class':'form-control'}),
		'direccion' : forms.TextInput(attrs={'class':'form-control'}),
		}
class FormularioRol(forms.ModelForm):
	class Meta:
		model = Rol
		fields=["nombre"]#lo que voy a representar en la parte grafica
		widgets = {
		
		'nombre' : forms.Select(attrs={'class':'form-control'}),
		
		}
class FormularioImagen(forms.ModelForm):
    class Meta:
        model = Imagen
        fields = ('titulo', 'descripcion', 'foto')
        #widgets = {
        #    'archivo': CustomClearableFileInput
        #}	

class FormularioAudio(forms.ModelForm):
    class Meta:
        model = Audio
        fields = ('titulo', 'descripcion', 'audio')	