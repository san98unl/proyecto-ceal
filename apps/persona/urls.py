from django.urls import path
from .import views


urlpatterns = [
	path('',views.principal, name="paciente"),
    path('crear_persona',views.crear_persona,name='crear_persona'),
    path('listar',views.listar_personas,name='listar_persona'),
    path('asignar_rol/',views.asignar_rol,name='asignar_rol'),
    path('buscar/',views.busqueda,name='buscar'),
    path('modificar_persona/',views.modificar_persona,name='modificar_persona'),
    path('eliminar_persona/',views.eliminar_persona,name='eliminar_persona'),

    path('imagenes',views.imagenes),
    path('audios',views.audios),

    path('guardar_imagen/',views.guardar_imagen),
     path('guardar_audio/',views.guardar_audio),
    
    #path('crear_medico',views.crear),
    
]