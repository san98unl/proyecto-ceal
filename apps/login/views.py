from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import FormularioLogin
from django.contrib.auth  import authenticate, login, logout
# Create your views here.
def ingresar(request):
	if request.method=='POST':
		formulario=FormularioLogin(request.POST)
		if formulario.is_valid():
			usuario = request.POST['username']#no se usa cleaned data xq no hay formulario
			clave = request.POST['password']
			user = authenticate(username = usuario, password = clave)#reviso que sea usuario
			if user is not None:
				if user.is_active:
					login(request,user)
					return HttpResponseRedirect(reverse('home_page'))#peticion de respueta
				else:
					print("usuario desacrivado")
			else:
				print("usuario y/o contraseña invalida")
			
	else:
		formulario=FormularioLogin()
	context={
	'formulario':formulario,

	}
	return render(request,'login.html',context)
def cerrar(request):
	logout(request)
	return HttpResponseRedirect(reverse('home_page'))
	