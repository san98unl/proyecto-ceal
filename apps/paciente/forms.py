from django import forms
from apps.modelo.models import Doctor
from apps.modelo.models import Paciente

from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField


#por cada metodo se coloca un url..
class FormularioPaciente(forms.ModelForm):
	class Meta:
		model = Paciente
		fields=["cedula","nombres","apellidos","genero"]#lo que voy a representar en la parte grafica
		widgets = {
		'cedula' : forms.TextInput(attrs={'class':'form-control'}),
		'nombres' : forms.TextInput(attrs={'class':'form-control'}),
		'apellidos' : forms.TextInput(attrs={'class':'form-control'}),
		'genero' : forms.Select(attrs={'class':'form-control'}),
		}
class FormularioModificarPaciente(forms.ModelForm):
	class Meta:
		model = Doctor
		fields=["nombres","apellidos","genero"]#lo que voy a representar en la parte grafica
		