from django.urls import path
from .import views


urlpatterns = [
	path('',views.principal, name="paciente"),
    path('crear_paciente/',views.crear_paciente),
    path('listar_pacientes',views.listar_pacientes),
    path('mostrar',views.mostrar, name="mostrar"),
    #path(r'^prueba/$',views.prueba, name="prueba"),
    path('buscar/',views.busqueda, name="busqueda"),
    path('modificar_paciente/',views.modificar_paciente),
    path('eliminar_paciente/',views.eliminar),
    
    #path('crear_medico',views.crear),
    
]