from django.shortcuts import render, redirect
from apps.modelo.models import Doctor,Paciente
from .forms import FormularioPaciente,FormularioModificarPaciente
#Para Buscar
from django.db.models import Q
#from django.shortcuts import render_to_response
#Para login
from django.contrib.auth.decorators import login_required


# Create your views here.

def prueba(request):
	queryset=request.GET.get("buscar")
	doc=Doctor.objects.all()
	if queryset:
		doc=Doctor.objects.filter(
			Q(cedula = queryset) 
			).distinct()

	return render (request,'paciente/busqueda.html',{'dat': doc})
@login_required
def mostrar(request):
	

	return render (request,'paciente/busqueda.html')#reder redireccion, rempitanta un docuemto html en forma grafica,,,escribimos el nombre de nuestra url
@login_required
def eliminar(request):
	dni = request.GET['paciente_id']
	paciente = Paciente.objects.get(paciente_id = dni)
	paciente.delete()

	return redirect(mostrar)

def busqueda(request):
    query = request.GET.get('q', '')

    print(query)
    if query:
        qset = (
            Q(cedula=query) 
        )

        results = Doctor.objects.filter(qset).distinct()
    else:
        results = []
    return render("paciente/busqueda.html", {
        "results": results,
        "query": query
    })

@login_required
def principal(request):
	

	return render (request,'principal.html')#reder redireccion, rempitanta un docuemto html en forma grafica,,,escribimos el nombre de nuestra url
@login_required
def listar_pacientes(request):
	lista=Paciente.objects.all()


	return render(request,'paciente/listar_paciente.html',{'lista_pacientes': lista})

def crear_paciente(request):
	dni = request.GET['cedula']
	doctor = Doctor.objects.get(cedula = dni)

	formulario = FormularioPaciente(request.POST)
		
	 
		
	if request.method == 'POST':
		if formulario.is_valid():	
				datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
				paciente=Paciente() #asi se crear los objetos en python
				paciente.cedula=datos.get('cedula')
				paciente.nombres=datos.get('nombres')
				paciente.apellidos=datos.get('apellidos')
				paciente.genero=datos.get('genero')
				paciente.doctor_id=doctor.doctor_id
				
				
				paciente.save()
				

		return redirect(principal)
	#else:
			#return render (request,'cliente/acceso_prohibido.html')	
	context={
			'f':formulario,
			

	}
	return render (request,'paciente/crear_paciente.html', context)
#Aun no implementado
@login_required
def modificar_paciente(request):
	dni = request.GET['cedula']
	paciente = Paciente.objects.get(cedula = dni)
	if request.method == 'POST': 
		formulario = FormularioModificarPaciente(request.POST)
		if formulario.is_valid():
			datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
			paciente.nombres=datos.get('nombres')
			paciente.apellidos=datos.get('apellidos')
			paciente.genero=datos.get('genero')
			
			paciente.save()
			return redirect(principal)

		#se modifica
		
	else:
		formulario = FormularioPaciente(instance = paciente)
	context = {
		'dni': dni,
		'doctor' : paciente,
		'formulario': formulario
	}
	return render (request,'paciente/modificar_paciente.html', context)