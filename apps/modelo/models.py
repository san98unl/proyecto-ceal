

from django.db import models

# Create your models here.
class Persona(models.Model):
	listaGenero=(
		('femenino','Femenino'),
		('masculino','Masculino'),#lista de generos para cargar el combo
		)
	
	persona_id = models.AutoField(primary_key = True)#es el auto incrementable
	cedula = models.CharField(max_length = 10, unique = True, null = False)
	nombres = models.CharField(max_length = 50, null = False)
	apellidos = models.CharField(max_length = 50, null = False)
	direccion = models.TextField(max_length = 50, default='sin direccion')
	telefono = models.CharField(max_length = 10)
	genero = models.CharField(max_length = 15, choices = listaGenero, default = 'Femenino' , null = False)
	correo = models.EmailField(max_length = 50, null = False)
	fechaNacimiento = models.DateField(auto_now=False, auto_now_add = False, null = False)
	celular = models.CharField(max_length = 10)
	rol=models.ForeignKey('Rol',default=1,verbose_name="Rol",on_delete=models.CASCADE)
class Rol(models.Model):
	listaRol=(
		('1','paciente'),
		('2','doctor'),#lista de generos para cargar el combo
		)
	rol_id = models.AutoField(primary_key = True)#es el auto incrementable
	
	
	nombre = models.CharField(max_length = 50,choices = listaRol, default = 'paciente', null = False)
	

class Doctor(models.Model):
	listaGenero=(
		('femenino','Femenino'),
		('masculino','Masculino'),#lista de generos para cargar el combo
		)
	
	doctor_id = models.AutoField(primary_key = True)#es el auto incrementable
	cedula = models.CharField(max_length = 10, unique = True, null = False)
	nombres = models.CharField(max_length = 50, null = False)
	apellidos = models.CharField(max_length = 50, null = False)
	direccion = models.TextField(max_length = 50, default='sin direccion')
	telefono = models.CharField(max_length = 10)
	genero = models.CharField(max_length = 15, choices = listaGenero, default = 'Femenino' , null = False)
	correo = models.EmailField(max_length = 50, null = False)
	fechaNacimiento = models.DateField(auto_now=False, auto_now_add = False, null = False)
	celular = models.CharField(max_length = 10)
	

class Paciente(models.Model):
	listaGenero=(
		('femenino','Femenino'),
		('masculino','Masculino'),#lista de generos para cargar el combo
		)
	paciente_id = models.AutoField(primary_key = True)
	cedula = models.CharField(max_length = 10, unique = True, null = False)
	nombres = models.CharField(max_length = 50, null = False)
	apellidos = models.CharField(max_length = 50, null = False)
	genero = models.CharField(max_length = 15, choices = listaGenero, default = 'Femenino' , null = False)
	doctor=models.ForeignKey('Doctor',default=1,verbose_name="Doctor",on_delete=models.CASCADE)
	#cliente = models.ForeignKey(
	#	'Cliente',#referencia al modelo
	#	on_delete=models.CASCADE,
	#	)
class Pictograma(models.Model):
	
	pictograma_id = models.AutoField(primary_key = True)
	titulo = models.CharField(max_length = 100, unique = True, null = True)
	descripcion = models.CharField(max_length = 100, unique = True, null = True)
	#foto = models.FileField(blank=True, null=False,upload_to='pictures')
	
	
	persona=models.ForeignKey('Persona',default=1,verbose_name="Persona",on_delete=models.CASCADE)
	imagen=models.ForeignKey('Imagen',default=1,verbose_name="Imagen",on_delete=models.CASCADE)
	audio=models.ForeignKey('Audio',default=1,verbose_name="Audio",on_delete=models.CASCADE)
	#paciente=models.ForeignKey('Paciente',default=1,verbose_name="Paciente",on_delete=models.CASCADE)
	

class Imagen(models.Model):
	
	imagen_id = models.AutoField(primary_key = True)
	titulo = models.CharField(max_length = 100, unique = True, null = True)
	descripcion = models.CharField(max_length = 100, unique = True, null = True)
	foto = models.FileField(blank=True, null=False,upload_to='imagenes')
	
	
	persona=models.ForeignKey('Persona',default=1,verbose_name="Persona",on_delete=models.CASCADE)
	
	

class Audio(models.Model):
	
	audio_id = models.AutoField(primary_key = True)
	titulo = models.CharField(max_length = 100, unique = True, null = True)
	descripcion = models.CharField(max_length = 100, unique = True, null = True)
	audio = models.FileField(blank=True, null=False,upload_to='audio')
	
	
	persona=models.ForeignKey('Persona',default=1,verbose_name="Persona",on_delete=models.CASCADE)
	#paciente=models.ForeignKey('Paciente',default=1,verbose_name="Paciente",on_delete=models.CASCADE)
