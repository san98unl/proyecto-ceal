# Generated by Django 2.2.5 on 2019-12-20 01:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('modelo', '0002_paciente_genero'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pictograma',
            fields=[
                ('pictograma_id', models.AutoField(primary_key=True, serialize=False)),
                ('titulo', models.CharField(max_length=100, null=True, unique=True)),
                ('descripcion', models.CharField(max_length=100, null=True, unique=True)),
                ('foto', models.FileField(blank=True, upload_to='pictures')),
                ('doctor', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='modelo.Doctor', verbose_name='Doctor')),
                ('paciente', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='modelo.Paciente', verbose_name='Paciente')),
            ],
        ),
    ]
