from django.contrib import admin

# Register your models here.
from.models import Doctor, Persona#realizar importaciones
from.models import Paciente, Pictograma,Audio, Imagen #realizar importaciones


class AdminDoctor(admin.ModelAdmin):
	list_display = ["cedula","apellidos","nombres","genero"]#permite realizar listas
	list_editable = ["apellidos","nombres"] #permite editar
	list_filter=["genero"]#son filtros
	search_fields=["cedula","apellidos"]#realiza las busquedas de los datos
	class Meta: #caracteristicas para mapear un modlo del crug
		model= Doctor
admin.site.register(Doctor, AdminDoctor)

class AdminPaciente(admin.ModelAdmin):
	list_display = ["doctor"]#permite realizar listas
	#list_editable = ["apellidos","nombres"] #permite editar
	list_filter=["doctor"]#son filtros
	search_fields=["numero"]

	class Meta: #caracteristicas para mapear un modlo del crug
		model= Paciente
admin.site.register(Paciente, AdminPaciente)

class AdminPictograma(admin.ModelAdmin):
	list_display = ["persona"]#permite realizar listas
	#list_editable = ["apellidos","nombres"] #permite editar
	list_filter=["persona"]#son filtros
	search_fields=["titulo"]

	class Meta: #caracteristicas para mapear un modlo del crug
		model= Pictograma
admin.site.register(Pictograma, AdminPictograma)

class AdminPersona(admin.ModelAdmin):
	list_display = ["cedula"]#permite realizar listas
	#list_editable = ["apellidos","nombres"] #permite editar
	list_filter=["rol"]#son filtros
	search_fields=["cedula"]

	class Meta: #caracteristicas para mapear un modlo del crug
		model= Persona
admin.site.register(Persona, AdminPersona)

class AdminAudio(admin.ModelAdmin):
	list_display = ["titulo"]#permite realizar listas
	#list_editable = ["apellidos","nombres"] #permite editar
	#list_filter=["rol"]#son filtros
	#search_fields=["cedula"]

	class Meta: #caracteristicas para mapear un modlo del crug
		model= Audio
admin.site.register(Audio, AdminAudio)

class AdminImagen(admin.ModelAdmin):
	list_display = ["titulo"]#permite realizar listas
	#list_editable = ["apellidos","nombres"] #permite editar
	#list_filter=["rol"]#son filtros
	#search_fields=["cedula"]

	class Meta: #caracteristicas para mapear un modlo del crug
		model= Imagen
admin.site.register(Imagen, AdminImagen)



	

	
		
